//
//  ViewController.swift
//  WeatherTableView
//
//  Created by Javier Ramon Gil on 28/4/15.
//  Copyright (c) 2015 Apress. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource {
    let cellTableIdentifier = "CellTableIdentifier"
    @IBOutlet var tableView:UITableView!
    
    var jsonResultsArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        tableView.registerClass(WeatherCell.self, forCellReuseIdentifier: cellTableIdentifier)
        let nib = UINib(nibName: "WeatherCell", bundle: nil)
        tableView.registerNib(nib, forCellReuseIdentifier: cellTableIdentifier)
        
        let jsonUrl: NSURL = NSURL(string: "http://pixel.uji.es/meteo/s_index.php?measure=1&sortasc=ASC&province=&limit=1000&hours=1&lat=40&lon=0")!
        let jsonResultsData = NSData(contentsOfURL: jsonUrl, options: NSDataReadingOptions.DataReadingUncached, error: nil)
        let jsonResultsString:NSString = NSString(data: jsonResultsData!, encoding: NSUTF8StringEncoding)!
        var jsonResultsDict:NSDictionary = NSJSONSerialization.JSONObjectWithData(jsonResultsData!, options: nil, error: nil) as NSDictionary
        jsonResultsArray = jsonResultsDict["data"] as NSArray
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return jsonResultsArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(cellTableIdentifier, forIndexPath: indexPath)
            as WeatherCell
        
        cell.name = jsonResultsArray[indexPath.row]["NAME"] as String
        cell.province = jsonResultsArray[indexPath.row]["PROVINCIA"] as String
        cell.T = jsonResultsArray[indexPath.row]["T"] as String
        var Pstr = jsonResultsArray[indexPath.row]["P"] as String
        
        var Pformatter = NSNumberFormatter().numberFromString(Pstr)
        var P = Pformatter!.intValue
        
        if (P == 0){
            cell.myImage.image = UIImage(named: "sun")
        } else if (P > 0 && P < 1){
            cell.myImage.image = UIImage(named: "rainy")
        }else{
            cell.myImage.image = UIImage(named: "rain")
        }
        
        return cell
    }
    

}

